import glob
import os

base_folder = "Results"
dataset_folders = os.listdir(base_folder)

results_per_dataset = {}
methods = ["MCSIntegratedTE", "MCSSeperateTE", "EMFCompare", "ConQat", "EScan", "ScanQat"]


class ExperimentResult:
    def __init__(self, precision, recall, f_measure, precision_num, recall_num, f_measure_num):
        self.precision = precision
        self.recall = recall
        self.f_measure = f_measure
        self.precision_num = precision_num
        self.recall_num = recall_num
        self.f_measure_num = f_measure_num


max_precision_per_dataset_mutation = {}
max_recall_per_dataset_mutation = {}
max_f_measure_per_dataset_mutation = {}

for dataset_folder in dataset_folders:
    current_path = base_folder + "/" + dataset_folder
    mutation_folders = os.listdir(current_path)
    for mutation_folder in mutation_folders:
        current_path = base_folder + "/" + dataset_folder + "/" + mutation_folder
        # Get the file with the average results of all approaches on one dataset
        result_files = glob.glob(current_path + "/*_AVG.txt")
        if len(result_files) != 1:
            continue
        else:
            result_file = result_files[0]

        # Initialize or load the dictionary with the results on this dataset
        temp = result_file.split("/")[-1]
        temp = temp[12:]
        dataset_name = temp.split("_")[0]
        if dataset_name in results_per_dataset:
            # We already have a dictionary
            results_per_method = results_per_dataset[dataset_name]
        else:
            # We have to create a new dictionary and add it to the dictionary with overall results
            results_per_method = {}
            results_per_dataset[dataset_name] = results_per_method

        with open(result_file, 'r') as f:
            results = f.readlines()

        for result in results:
            parts = result.split("&")
            # Get the important information from the line
            method_name = parts[0].strip()
            # Skip if this method is not in the ones we want to evaluate
            if method_name not in methods:
                continue
            precision_num = float(parts[-4].strip())
            recall_num = float(parts[-3].strip())
            f_measure_num = float(parts[-2].strip())

            # Next, we want to store the final experiment result in the results per mutation
            mutation_count = mutation_folder.split()[0]
            key = dataset_name + mutation_count

            # Change format of numbers to x.xx
            if key in max_precision_per_dataset_mutation:
                if precision_num >= max_precision_per_dataset_mutation[key]:
                    max_precision_per_dataset_mutation[key] = precision_num
            else:
                max_precision_per_dataset_mutation[key] = precision_num

            if key in max_recall_per_dataset_mutation:
                if recall_num >= max_recall_per_dataset_mutation[key]:
                    max_recall_per_dataset_mutation[key] = recall_num
            else:
                max_recall_per_dataset_mutation[key] = recall_num

            if key in max_f_measure_per_dataset_mutation:
                if f_measure_num >= max_f_measure_per_dataset_mutation[key]:
                    max_f_measure_per_dataset_mutation[key] = f_measure_num
            else:
                max_f_measure_per_dataset_mutation[key] = f_measure_num

            precision = "{0:.2f}".format(precision_num)
            recall = "{0:.2f}".format(recall_num)
            f_measure = "{0:.2f}".format(f_measure_num)

            # We now load the dictionary with the results of this method for a specific mutation number, or initialize
            if method_name in results_per_method:
                results_per_mutation = results_per_method[method_name]
            else:
                results_per_mutation = {}
                results_per_method[method_name] = results_per_mutation

            if mutation_count in results_per_mutation:
                raise RuntimeError()
            else:
                final_result = ExperimentResult(precision=precision, recall=recall, f_measure=f_measure,
                                                precision_num=precision_num, recall_num=recall_num,
                                                f_measure_num=f_measure_num)
                results_per_mutation[mutation_count] = final_result

# We should now have the results loaded in the following form
"""
results_per_dataset:
    fmedit-atomic -> results_per_method:
                        MCSCTE -> results_per_mutation:
                                    0 -> ExperimentResult
                                    1 -> ExperimentResult
                                    2 -> ExperimentResult
                                    3 -> ExperimentResult
                                    4 -> ExperimentResult
                                    5 -> ExperimentResult
                        EMFComp -> results_per_mutation:
                                    ...
                                    ...
                        ...
                        ...
    fmedit-complex -> results_per_method:
                        ...
                        ...
    ...
    ...
"""
# Now we want to create the table with only the best methods and 0, 1, and 3 mutations
mutations = ["0", "1", "3"]
datasets = ["fmedit-atomic", "fmedit-complex", "fmrecog-atomic", "fmrecog-complex", 'ocl2ngc', 'umledit-generated',
            'umledit-manual', 'umlrecog-generated', 'umlrecog-manual']

header_lines = {'fmedit-atomic'
                : "\\multicolumn{10}{c}{\\textbf{fm-edit-atomic - 27 rules, 5.7 Nodes, 4.6 Edges}} \\\\",
                'fmedit-complex'
                : "\\multicolumn{10}{c}{\\textbf{fm-edit-complex - 31 rules, 11.5 Nodes, 15.6 Edges}} \\\\",
                'fmrecog-atomic'
                : "\\multicolumn{10}{c}{\\textbf{fm-recog-atomic - 27 rules, 21.7 Nodes, 29.6 Edges}} \\\\ ",
                'fmrecog-complex'
                : "\\multicolumn{10}{c}{\\textbf{fm-recog-complex - 31 rules, 58.65 Nodes, 111.39 Edges}} \\\\",
                'ocl2ngc'
                : "\\multicolumn{10}{c}{\\textbf{ocl2ngc - 52 rules, 31.9 Nodes, 42.5 Edges}} \\\\",
                'umledit-generated'
                : "\\multicolumn{10}{c}{\\textbf{uml-edit-generated - 1379 rules, 4.6 Nodes, 1.9 Edges}} \\\\",
                'umledit-manual'
                : "\\multicolumn{10}{c}{\\textbf{uml-edit-manual - 25 rules, 5.2 Nodes, 4.1 Edges}} \\\\",
                'umlrecog-generated'
                : "\\multicolumn{10}{c}{\\textbf{uml-recog-generated - 1379 rules, 18.2 Nodes, 21.5 Edges}} \\\\",
                'umlrecog-manual'
                : "\\multicolumn{10}{c}{\\textbf{uml-recog-manual - 25 rules, 26.6 Nodes, 42.2 Edges}} \\\\"}

method_aliases = {"MCSIntegratedTE": "MCS-Integrated-TE",
                  "MCSSeperateTE": "MCS-Separate-TE",
                  "EMFCompare": "EMFCompare",
                  "ConQat": "ConQat",
                  "EScan": "EScan",
                  "ScanQat": "ScanQat"}

# Load the template files for the table
template_part1 = ""
with open("table_template_part1", 'r') as f:
    for line in f.readlines():
        template_part1 += line + "\r\n"
template_part2 = ""
with open("table_template_part2", 'r') as f:
    for line in f.readlines():
        template_part2 += line + "\r\n"

table = template_part1

for dataset in datasets:
    if dataset in results_per_dataset:
        # Add the multicolumn header for the datset
        table += header_lines[dataset] + "\r\n"
        table += "\r\n"
        # Iterate over the methods to get their results per number of mutation
        results_per_method = results_per_dataset[dataset]
        for method in methods:
            results_per_mutation = results_per_method[method]
            table += method_aliases[method]
            for mutation in mutations:
                final_result = results_per_mutation[mutation]  # type: ExperimentResult
                key = dataset + mutation
                if final_result.precision_num >= max_precision_per_dataset_mutation[key]:
                    final_result.precision = "\\textbf{" + final_result.precision + "}"
                if final_result.recall_num >= max_recall_per_dataset_mutation[key]:
                    final_result.recall = "\\textbf{" + final_result.recall + "}"
                if final_result.f_measure_num >= max_f_measure_per_dataset_mutation[key]:
                    final_result.f_measure = "\\textbf{" + final_result.f_measure + "}"

                result_line = "~ & " + final_result.precision \
                              + "~ & " + final_result.recall \
                              + "~ & " + final_result.f_measure
                table += result_line
            table += " \\\\"
            table += "\r\n"
        table += "\r\n"
        table += "\r\n"
# Add the second template to finish the table
table += template_part2
print(table)

path_to_table = "../ECMFA2020/content/table2.tex"
with open(path_to_table, 'w') as f:
    f.writelines(table)
