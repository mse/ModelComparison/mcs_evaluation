import matplotlib.pyplot as plt
import numpy as np
import matplotlib.patches as mpatches

folder = "Test_Timeout"

result_file = folder + "/" + "timeout-rules_TIME.txt"

with open(result_file, 'r') as f:
    result_lines = f.readlines()

f_measure_per_rule = {}
timeout_per_rule = {}

colors = ["red", "blue", "green", "orange", "purple"]

result_lines = result_lines[1:]
for line in result_lines:
    parts = line.split("&")
    name = parts[-3].strip()
    if name in f_measure_per_rule:
        f_measures = f_measure_per_rule[name]
        timeouts = timeout_per_rule[name]
    else:
        f_measures = []
        timeouts = []
        f_measure_per_rule[name] = f_measures
        timeout_per_rule[name] = timeouts
    f_measures.append(float(parts[-2].strip()))
    timeouts.append(int(parts[-1].strip()))

fig, ax = plt.subplots()
ax.set_title("")
labels = []
for index, rule in enumerate(f_measure_per_rule.keys()):
    f_measures = f_measure_per_rule[rule]
    timeouts = timeout_per_rule[rule]
    sort_order = np.argsort(timeouts)

    temp_f = [float(0) for x in range(0, len(f_measures))]
    temp_t = [0 for x in range(0, len(f_measures))]
    for i in range(0, len(f_measures)):
        temp_t[i] = timeouts[sort_order[i]]
        temp_f[i] = f_measures[sort_order[i]]
    f_measures = temp_f
    timeouts = temp_t

    min_timeout = 0
    max_timeout = 1000
    step_size = 200
    x_labels = [x for x in range(min_timeout, max_timeout+1, step_size)]
    y_labels = ["0.00", "0.10", "0.20", "0.30", "0.40", "0.50", "0.60", "0.70", "0.80", "0.90", "1.00"]

    ax.scatter(timeouts, f_measures, marker=".")

    plt.xlabel("Timeout [ms]")
    plt.ylabel("F-Measure")
    plt.xticks(np.arange(min_timeout, max_timeout+1, step=step_size), x_labels)
    plt.yticks(np.arange(0, 1.01, step=0.1), y_labels)

    labels.append(mpatches.Patch(color=colors[index], label=rule[8:]))
    plt.legend(handles=labels, loc=4)
plt.show()
#fig.savefig("../ECMFA2020/img/timeouts.pdf")

print("Success!")