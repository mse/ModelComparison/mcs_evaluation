import os, csv

tmp_file = 'tmp'
engines = [("MCSIntegratedTE", "Integrated-TE"), ("MCSIntegratedTC","Integrated-TC"),
           ("MCSIntegratedTEAE","Integrated-TE-AE"), ("MCSIntegratedTCAE", "Integrated-TC-AE"),
           ("MCSSeperateTE", "Separate-TE"), ("MCSSeperateTC", "Separate-TC"),
           ("EMFCompare", "EMFCompare"), ("ConQat", "ConQat"), ("EScan", "EScan"), ("ScanQat", "ScanQat")]

rule_lines = {}

def avg(ls):
    ret = []
    for l in ls:
        ret.append(round(sum(ls[l]) / len(ls[l]), 2))
    return ret


class Engine:
    def __init__(self, short_name, tex_name):
        self.short_name = short_name
        self.tex_name = tex_name
        self.precs, self.recs, self.fms = {}, {}, {}

    def add_values(self, values, mutations):
        (prec, rec, fm) = values
        if not mutations in self.precs:
            self.precs[mutations] = []
            self.recs[mutations] = []
            self.fms[mutations] = []
        self.precs[mutations].append(float(prec))
        self.recs[mutations].append(float(rec))
        self.fms[mutations].append(float(fm))

    def table1_line(self):
        #print(self.precs)
        aprec = avg(self.precs)
        arec = avg(self.recs)
        afm = avg(self.fms)
        mid_string = ""
        valid_mutations = [0, 1, 3]
        for i in valid_mutations:
            mid_string += "~ & " + format(aprec[i], '.2f') + "~ & " + format(arec[i], '.2f') + "~ & " + format(afm[i], '.2f')
        return self.tex_name + str(mid_string) + " \\\\"


engines = [Engine(e[0], e[1]) for e in engines]


class Rule:
    def __init__(self, name, nodes, edges):
        self.name = name
        self.nodes = nodes
        self.edges = edges


def add_lines(reader):
    pass


def main():
    global engines
    os.system('dir /s/b *_AVG.csv > ' + tmp_file)
    csv_files = [file[:-1] for file in open(tmp_file, 'r').readlines()]
    os.remove(tmp_file)
    for csv_file in csv_files:
        print("Parsing " + csv_file + " ...")
        mutations = int(csv_file.split("\\")[-2][0])

        with open(csv_file, newline='') as file:
            reader = csv.reader(file, delimiter=';')
            next(reader, None)
            for row in reader:
                for E in engines:
                    if E.short_name == row[0]:
                        values = [i[2:-1] for i in row[6:9]]
                        E.add_values(values, mutations)
    print("Parsed " + str(len(csv_files)) + " files.")
    print("\n Table 1:")
    for E in engines[:-4]:
        print(E.table1_line())


if __name__ == "__main__":
    main()